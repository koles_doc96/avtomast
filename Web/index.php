<?php
/**
 * Created by PhpStorm.
 * User: Evgeniy
 * Date: 28.02.2018
 * Time: 12:49
 */
use Doctrine\DBAL\Connection;
use Silex\Application;
use Silex\Provider\DoctrineServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

// Для php -S host:port -t web web/index.php, чтобы статика отдавалась
if (php_sapi_name() === 'cli-server' && is_file(__DIR__ . preg_replace('/(\?.*)$/', '', $_SERVER['REQUEST_URI']))) {
    return false;
}

require_once __DIR__ . '/../vendor/autoload.php';

Request::enableHttpMethodParameterOverride();

$login; // 0 - просмотр не регестрированым пользоватиелям 1- зарегестрированый пользователь 999 - Админ
$NAME;

$sapp = (new Application(['debug' => true]))
    // шаблонизатор
    ->register(new TwigServiceProvider(),
        ['twig.path' => __DIR__ . '/../src/views'])
    // база
    ->register(new DoctrineServiceProvider(),
        ['db.options' => ['driver' => 'pdo_mysql', 'dbname' => 'Avtomaster', 'charset' => 'utf8']]);

$sapp->get('/', function (Application $app) {
    session_start();
    /**@var $conn Connection */
    $conn = $app['db'];
    $id_client =$_SESSION['id'];
    $Services = $conn->fetchAll('select * from Services');
    $car_repair = $conn->fetchAll('select * from Car_Repair_shop');
    $servic = $conn->fetchAll("SELECT * FROM Order_Client where  Id_Client = ?", [$id_client]);
    $id_order = $servic[0]["Id_Order"];

    $foter=1;

    if($_SESSION['auth']==true) {
        $NAME = $_SESSION['login'];
        $login =$_SESSION['priority'] ;


    }
    return $app['twig']->render('Services.twig', ['car_repair' => $car_repair,'Services' => $Services,'foter'=>$foter,'login'=>$login,'name'=>$NAME,'id_order'=>$id_order]);
});
// Регистрация
$sapp->get('/regestration/', function (Application $app) {
    session_start();
    /**@var $conn Connection */
    $conn = $app['db'];

    return $app['twig']->render('Regestration.twig', ['foter'=>1,'login'=>$_SESSION['priority'],'name'=>$_SESSION['login']]);
});
// Вывод купленных товаров
$sapp->get('/bausket/', function (Application $app) {
    session_start();
    /**@var $conn Connection */
    $conn = $app['db'];
    $id_client =$_SESSION['id'];
    $servic = $conn->fetchAll("SELECT * FROM Order_Client where  Id_Client = ?", [$id_client]);
    $id_order = $servic[0]["Id_Order"];
    $Services = $conn->fetchAll('select * from Services WHERE  Id_Order = ?',[$id_order]);
    $foter=1;
    $sum=0;
    for($i =0;$i<count($Services);$i++) {
        $dop = (int)$Services[$i]["Price"];
        $sum+=$dop;
    }

    return $app['twig']->render('busket.twig', ['Services' => $Services,'foter'=>$foter,'login'=>$_SESSION['priority'],'name'=>$_SESSION['login'],'sum'=>$sum]);
});

$sapp->get('/service/{id}', function ($id) use ($sapp) {
    /**@var $conn Connection */
    $conn = $sapp['db'];
    session_start();
    $servic = $conn->fetchAll("select * from Services where Id_Servisces = ?", [$id]);
    if(!$servic)
    {
        throw new NotFoundHttpException("Нет такой услуги-$id");
    }
    return $sapp['twig']->render('service.twig', ['servic' => $servic,'login'=>$_SESSION['priority'],'name'=>$_SESSION['login']]);
});
$sapp->get('/services/add/', function (Application $app) {
    /**@var $conn Connection */
    $conn = $app['db'];
    $car_repair = $conn->fetchAll('select * from Car_Repair_shop');
    $editstyle=0;
    return $app['twig']->render('EditPage.twig', ['car_repair' => $car_repair,'edit_style' =>$editstyle]);
});
$sapp->get('/lastService', function (Application $app) {
    /**@var $conn Connection */
    $conn = $app['db'];
    $services = $conn->fetchAll('SELECT * FROM Services ORDER BY Id_Servisces DESC LIMIT 1');
    return $app['twig']->render('/', ['service' => $services]);
});

//$sapp->post('/services/add', function (Request $req) use ($sapp) {
//    /**@var $conn Connection */
//    $conn = $sapp['db'];
//    $name = $req->get('name');
//    $description = $req->get('description');
//    $price = $req->get('price');
//    $id_car_repair_shop=$req->get('id_car_repair_shop');
//    $conn->insert('Services', ['name' => $name,'Description'=>$description,'Price'=>$price,'Id_Car_reapair_shop'=>$id_car_repair_shop]);
//    return $sapp->redirect('/');
//});
$sapp->post('/services/add', function (Request $req) use ($sapp) {
    /**@var $conn Connection */
    $conn = $sapp['db'];
    $name = $req->get('name');
    $description = $req->get('description');
    $price = $req->get('price');
    $id_car_repair_shop=$req->get('id_car_repair_shop');
    $conn->insert('Services', ['name' => $name,'Description'=>$description,'Price'=>$price,'Id_Car_reapair_shop'=>$id_car_repair_shop]);

    $services = $conn->fetchAll('SELECT * FROM Services ORDER BY Id_Servisces DESC LIMIT 1');
    return $sapp->json(array('serv'=>$services), 200);
});
// Регистрация клиента
$sapp->post('/addClient/', function (Request $req) use ($sapp) {
    /**@var $conn Connection */
    $conn = $sapp['db'];
    session_start();
    $FIO = $req->get('FIO');
    $login = $req->get('login');
    $password = $req->get('password');
    $clients = $conn->fetchAll('SELECT * FROM Client');
    for($i =0;$i<count($clients);$i++) {
        if ($clients[$i]["login"] == $login) {

            return $sapp['twig']->render('Regestration.twig', ['error' => 0,'foter'=>1,'login'=>$_SESSION['priority'],'name'=>$_SESSION['login']]);
        }
    }
    $conn->insert('Client', ['FIO' => $FIO,'FIO'=>$login,'login'=>$login,'password'=>$password]);
    $client = $conn->fetchAll('SELECT * FROM Client ORDER BY Id_Client DESC LIMIT 1');
    $id_client = $client[0]["Id_Client"];
    $_SESSION['id'] = $id_client;
    $_SESSION['login'] = $FIO;
    $_SESSION['auth'] = true;
    $_SESSION['priority']=1;
    $conn->insert('Order_Client', ['Id_Client' => $id_client]);
    return $sapp->redirect('/');
});
//login/
$sapp->get('/login/', function (Request $req) use ($sapp) {
    /**@var $conn Connection */
    session_start();
    $conn = $sapp['db'];
    $name = $req->get('login');
    $password=$req->get('password');
    global $login,$NAME;
    $clients = $conn->fetchAll('SELECT * FROM Client');
    for($i =0;$i<count($clients);$i++) {
        if ($clients[$i]["login"] == $name and $clients[$i]["password"] == $password) {
            $_SESSION['priority']=1;
            $_SESSION['auth'] = true;
            $_SESSION['id'] = $clients[$i]["Id_Client"];
            $_SESSION['login'] = $clients[$i]["FIO"];
            break;
        }
        elseif ($name=="root" and $password =="root")
        {
            $_SESSION['priority']=999;
            $_SESSION['auth'] = true;
            $_SESSION['login'] = "Администратор";
        }

    }
    return $sapp->redirect('/');
});
$sapp->post('/logout/', function (Request $req) use ($sapp) {
    /**@var $conn Connection */
    session_start();
    $conn = $sapp['db'];
    if (!empty($_SESSION['auth']) and $_SESSION['auth']) {
        session_start();
        session_destroy(); //разрушаем сессию для пользователя
    }
    return $sapp->redirect('/');
});
$sapp->POST('/services/{id}', function ($id) use ($sapp) {
    /**@var $conn Connection */
    $conn = $sapp['db'];
    $conn->delete('Services', ['Id_Servisces' => $id]);
    return $sapp->redirect('/');
});
// Добавление в корзину
$sapp->POST('/addBusket/{id}', function ($id) use ($sapp) {
    /**@var $conn Connection */
    $conn = $sapp['db'];
    session_start();
    $id_client =$_SESSION['id'];
    $servic = $conn->fetchAll("SELECT * FROM Order_Client where  Id_Client = ?", [$id_client]);
    $update = $conn->prepare("UPDATE Services  set id_order =:id_order   where Id_Servisces=:id");
    $update->bindParam(':id_order', $id_order);
    $update->bindParam(':id', $id);
    $id_order = $servic[0]["Id_Order"];
    $_SESSION["id_order"] = $id_order;
    $update->execute();
    return $sapp->redirect('/');
});
// удаление из корзины
$sapp->Get('/dell/{id}', function ($id) use ($sapp) {
    /**@var $conn Connection */
    $conn = $sapp['db'];
    session_start();
    $id_client =$_SESSION['id'];
    $servic = $conn->fetchAll("SELECT * FROM Order_Client where  Id_Client = ?", [$id_client]);
    $update = $conn->prepare("UPDATE Services  set id_order =:id_order   where Id_Servisces=:id");
    $update->bindParam(':id_order', $id_order);
    $update->bindParam(':id', $id);
    $id_order = -999;
    $update->execute();
    return $sapp->redirect('/bausket/');
});


$sapp->DELETE('/services/{id}', function ($id) use ($sapp) {
    /**@var $conn Connection */
    $conn = $sapp['db'];
    $conn->delete('Services', ['Id_Servisces' => $id]);
    return $sapp->redirect('/');
});
$sapp->POST('/service/{id}/', function (Request $request,$id) use ($sapp) {
    /**@var $conn Connection */
    $conn = $sapp['db'];
    $update = $conn->prepare("UPDATE Services set name=:name, Description = :description, Price = :price,Id_Car_reapair_shop=:Id_Car_reapair_shop   where Id_Servisces=:id");

    $update->bindParam(':id', $id);
    $update->bindParam(':name', $name);
    $update->bindParam(':description', $description);
    $update->bindParam(':price', $price);
    $update->bindParam(':Id_Car_reapair_shop', $id_Car_reapair_shop);

    $name = $request->get('name');
    $description = $request->get('description');
    $price = $request->get('price');
    $id_Car_reapair_shop = $request->get('id_car_repair_shop');

    $update->execute();


    return $sapp->redirect("/service/$id");
});

$sapp->get('/service/up/{id}/', function ($id) use ($sapp) {
    /**@var $conn Connection */
    $conn = $sapp['db'];
    $service = $conn->fetchAll("select * from Services where Id_Servisces = ?", [$id]);
    $car_repair = $conn->fetchAll('select * from Car_Repair_shop');
    $editstyle=1;
    return $sapp['twig']->render('EditPage.twig', ['edit_style' =>$editstyle,'services'=>$service,'car_repair'=>$car_repair,'login'=>$_SESSION['priority'],'name'=>$_SESSION['login']]);
});
$sapp->run();